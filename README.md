# Smarter integration testing with TestKit

This is an example of using TestKit in an integration test.

To learn more about TestKit and writing integration tests for Jira please visit: 
[Tutorial - Smarter integration testing with TestKit][1].

## Running locally

To run this app locally, make sure that you have the Atlassian Plugin SDK installed, and then run:

    atlas-mvn jira:run

 [1]: https://developer.atlassian.com/jiradev/jira-platform/guides/other/tutorial-smarter-integration-testing-with-testkit
